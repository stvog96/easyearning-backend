﻿namespace Model.Interface
{
    public interface IUserModel
    {
        string UserName { get; set; }
        string UserEmail { get; set; }
        string UserToken { get; set; }
        int UserCoins { get; set; }
    }
}
