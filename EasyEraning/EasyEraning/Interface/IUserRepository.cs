﻿using Model.Implimentation;

namespace EasyEraning.Interface
{
    public interface IUserRepository
    {
        public UserModel GetUser(UserModel user);
        bool CheckIfUserExist(UserModel user);
        UserModel CreateNewuser(UserModel user);
    }
}
