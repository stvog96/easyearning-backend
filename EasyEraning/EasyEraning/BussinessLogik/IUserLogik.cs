﻿using EasyEraning.Model.Implimentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyEraning.BussinessLogik
{
    public interface IUserLogik
    {
        public GoogleUserModel VerifyToken(string token);
    }
}
