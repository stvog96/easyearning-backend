﻿using EasyEraning.Model.Implimentation;
using Model.Implimentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyEraning.BussinessLogik
{
    public class UserLogik : IUserLogik
    {
        public UserLogik()
        {

        }

        public GoogleUserModel VerifyToken(string token)
        {
            var user = new GoogleUserModel();
            var client = new HttpClient();

            var verifyTokenEndPoint = string.Format("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token={0}", token);

            var uri = new Uri(verifyTokenEndPoint);
            var response = client.GetAsync(uri);

            if (response.Result.IsSuccessStatusCode)
            {
                var content = response.Result.Content.ReadAsStringAsync();
                dynamic userObj = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(content.Result);
                user = Newtonsoft.Json.JsonConvert.DeserializeObject<GoogleUserModel>(content.Result);
                return user;
            }
            return null;
        }
    }
}
