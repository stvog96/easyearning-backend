﻿using EasyEraning.BussinessLogik;
using EasyEraning.Interface;
using Microsoft.AspNetCore.Mvc;
using Model.Implimentation;

namespace EasyEraning.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserLogik _userLogik;
        public UserController(IUserRepository userRepository, IUserLogik userLogik)
        {
            _userRepository = userRepository;
            _userLogik = userLogik;
        }

        [HttpPost(nameof(SignInUser))]
        public UserModel SignInUser([FromBody]UserModel resutl)
        {
            try
            {
                if (_userLogik.VerifyToken(resutl.UserToken) != null)
                {
                    var bolean = _userRepository.CheckIfUserExist(resutl);
                    if (bolean)
                    {
                        return _userRepository.GetUser(resutl);
                    }
                    else
                    {
                        return _userRepository.CreateNewuser(resutl);
                    }
                }
                else
                    return null;

            }
            catch (System.Exception)
            {
                return null;
            }

        }

        [HttpGet("Hallo")]
        public void Hallo()
        {

        }

    }
}