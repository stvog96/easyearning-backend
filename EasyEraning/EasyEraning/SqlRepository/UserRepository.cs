﻿using EasyEraning.Interface;
using Model.Implimentation;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;

namespace EasyEraning.SqlRepository
{
    public class UserRepository : IUserRepository
    {

        public UserRepository()
        {

        }

        public bool CheckIfUserExist(UserModel user)
        {
            string result = "";
            using var con = new MySqlConnection(Const.eee);
            con.Open();
            using(var cmd = new MySqlCommand($"SELECT EXISTS(SELECT UserId From usertable WHERE UserId='{user.UserId}') as truth", con))
            {
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result = reader["truth"].ToString();
                }

                if (result == "1")
                    return true;
                else
                    return false;
            }
        }

        public UserModel CreateNewuser(UserModel user)
        {
            user.UserCoins = 500;
            user.UserApi_Key = GenerateKey() + user.UserToken;
            using var con = new MySqlConnection(Const.eee);
            con.Open();
            var cmd = new MySqlCommand($"INSERT INTO `usertable`(`UserName`, `UserEmail`, `UserCoins`,`UserApiKey`,`UserId`) VALUES ('{user.UserName}','{user.UserEmail}','{user.UserCoins}','{user.UserApi_Key}','{user.UserId}')", con);
            cmd.ExecuteNonQuery();
            con.Close();
            return user;
        }

        public UserModel GetUser(UserModel user)
        {
            using var con = new MySqlConnection(Const.eee);
            con.Open();
            using(var cmd = new MySqlCommand($"Select UserCoins,UserApiKey,UserDbId from UserTable Where UserId='{user.UserId}'", con))
            {
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    user.UserCoins = Convert.ToInt32(reader["UserCoins"]);
                    user.UserApi_Key = reader["UserApiKey"].ToString();
                    user.UserID_Db = reader["UserDbId"].ToString();
                }
            }
            return user;
        }

        private string GenerateKey()
        {
            var key = new byte[128];
            using (var generator = RandomNumberGenerator.Create())
                generator.GetBytes(key);
            string apiKey = Convert.ToBase64String(key);
            return apiKey;
        }
    }
}
